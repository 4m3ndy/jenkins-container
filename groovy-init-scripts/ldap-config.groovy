import jenkins.model.*
import hudson.security.*
import org.jenkinsci.plugins.*

String server = 'ldap://13.58.41.184:389'
String rootDN = 'dc=icssolutions,dc=ca'
String userSearchBase = 'ou=Users'
String userSearch = 'cn={0}'
String groupSearchBase = 'ou=Groups'
String managerDN = 'cn=admin,dc=icssolutions,dc=ca'
String managerPassword = 'qWerty1@3456'
boolean inhibitInferRootDN = false
 
SecurityRealm ldap_realm = new LDAPSecurityRealm(server, rootDN, userSearchBase, userSearch, groupSearchBase, managerDN, managerPassword, inhibitInferRootDN) 
Jenkins.instance.setSecurityRealm(ldap_realm)
Jenkins.instance.save()
