FROM jenkins/jenkins:lts

ENV JENKINS_USER jenkins.user
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false

RUN /usr/local/bin/install-plugins.sh ldap:latest git matrix-auth workflow-aggregator docker-workflow blueocean credentials-binding

COPY groovy-init-scripts/admin-user.groovy /usr/share/jenkins/ref/init.groovy.d/
COPY groovy-init-scripts/ldap-config.groovy /usr/share/jenkins/ref/init.groovy.d/
