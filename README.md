# Jenkins in a container

## Overview

Create an automated setup for Jenkins Master and Slaves using containers.  
Enable Jenkins authentication with an active directory.  
Document setup using readme.md.  

## AD Settings

* Username: `jenkins.user`
* Password: `qWerty1@3456`
* Ldap DN: `ou=Users,dc=icssolutions,dc=ca`
* Ldap End-Point: ldap://13.58.41.184:389

## How to build and run Docker Image
`docker build . -f Dockerfile -t jenkins-lts:ldap`
`docker run -p 8080:8080 -p 50000:50000 jenkins-lts:ldap`

Finally autheticate with `jenkins.user` credentials and make sure the user exists in the LDAP tree.

## How to contribute

1. Fork the repo
1. Make changes and commit to your own repository *new branch*
1. Submit a pull request back into this repository using the *new branch* name
